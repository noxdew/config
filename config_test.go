package config

import (
	"testing"

	assert "github.com/stretchr/testify/assert"
	suite "github.com/stretchr/testify/suite"
)

//go:generate mockery -all -inpkg

type CustomConfig struct {
	SomeKey string `yaml:"someKey"`
}

type TestSuite struct {
	suite.Suite
	ma *mockAccessor
}

func (suite *TestSuite) SetupTest() {
	suite.ma = new(mockAccessor)
}

func (suite *TestSuite) TestResolveFilePathAndParserDefaultsEnvironment() {
	suite.ma.On("Getenv", "ENV").Return("")
	suite.ma.On("Getenv", "CONFIG_DIR").Return("")
	suite.ma.On("Exists", "config/prod.yml").Return(true)

	env, path, parser := resolveFilePathAndParser(suite.ma)

	suite.ma.AssertExpectations(suite.T())
	assert.Equal(suite.T(), "prod", env)
	assert.Equal(suite.T(), "config/prod.yml", path)
	assert.IsType(suite.T(), yamlParser{}, parser)
}

func (suite *TestSuite) TestResolveFilePathAndParserReadsEnvironmentEnvironment() {
	suite.ma.On("Getenv", "ENV").Return("sandbox")
	suite.ma.On("Getenv", "CONFIG_DIR").Return("/some/dir")
	suite.ma.On("Exists", "/some/dir/sandbox.yml").Return(true)

	env, path, parser := resolveFilePathAndParser(suite.ma)

	suite.ma.AssertExpectations(suite.T())
	assert.Equal(suite.T(), "sandbox", env)
	assert.Equal(suite.T(), "/some/dir/sandbox.yml", path)
	assert.IsType(suite.T(), yamlParser{}, parser)
}

func (suite *TestSuite) TestResolveFilePathAndParserReturnsYamlParserForYamlFiles() {
	suite.ma.On("Getenv", "ENV").Return("sandbox")
	suite.ma.On("Getenv", "CONFIG_DIR").Return("/some/dir/")
	suite.ma.On("Exists", "/some/dir/sandbox.yml").Return(true)

	env, path, parser := resolveFilePathAndParser(suite.ma)

	suite.ma.AssertExpectations(suite.T())
	assert.Equal(suite.T(), "sandbox", env)
	assert.Equal(suite.T(), "/some/dir/sandbox.yml", path)
	assert.IsType(suite.T(), yamlParser{}, parser)
}

func (suite *TestSuite) TestResolveFilePathAndParserReturnsJsonParserForJsonFiles() {
	suite.ma.On("Getenv", "ENV").Return("sandbox")
	suite.ma.On("Getenv", "CONFIG_DIR").Return("")
	suite.ma.On("Exists", "config/sandbox.yml").Return(false)
	suite.ma.On("Exists", "config/sandbox.json").Return(true)

	env, path, parser := resolveFilePathAndParser(suite.ma)

	suite.ma.AssertExpectations(suite.T())
	assert.Equal(suite.T(), "sandbox", env)
	assert.Equal(suite.T(), "config/sandbox.json", path)
	assert.IsType(suite.T(), jsonParser{}, parser)
}

func (suite *TestSuite) TestResolveFilePathAndParserPanicsIfNoFilePresent() {
	suite.ma.On("Getenv", "ENV").Return("sandbox")
	suite.ma.On("Getenv", "CONFIG_DIR").Return("")
	suite.ma.On("Exists", "config/sandbox.yml").Return(false)
	suite.ma.On("Exists", "config/sandbox.json").Return(false)

	defer func() {
		if r := recover(); r == nil {
			suite.T().Errorf("The code did not panic")
		}
	}()

	resolveFilePathAndParser(suite.ma)
}

func (suite *TestSuite) TestLoadParsesConfigWithCustomNilYaml() {
	yaml := `
serviceName: test
port: 8081
env: env overwrite
tls:
  key: some key
  cert: some cert
custom:
  someKey: test
`
	env := "sandbox"
	path := "config/" + env + ".yml"
	suite.ma.On("Getenv", "ENV").Return(env)
	suite.ma.On("Getenv", "CONFIG_DIR").Return("")
	suite.ma.On("Exists", path).Return(true)
	suite.ma.On("ReadFile", path).Return([]byte(yaml), nil)

	conf := load(suite.ma, nil)

	suite.ma.AssertExpectations(suite.T())
	assert.Equal(suite.T(), env, conf.Env, "Env must be populated in the config from the ENV not the yaml")
	assert.Equal(suite.T(), "test", conf.ServiceName, "Incorrect population of the config from the yaml - serviceName")
	assert.Equal(suite.T(), 8081, conf.Port, "Incorrect population of the config from the yaml - port")
	assert.Equal(suite.T(), "some key", conf.TLS.Key, "Incorrect population of the config from the yaml - tls.key")
	assert.Equal(suite.T(), "some cert", conf.TLS.Cert, "Incorrect population of the config from the yaml - tls.cert")
}

func (suite *TestSuite) TestLoadParsesCustomConfigYaml() {
	yaml := `
custom:
  someKey: test
`
	env := "sandbox"
	path := "config/" + env + ".yml"
	cc := CustomConfig{}
	suite.ma.On("Getenv", "ENV").Return(env)
	suite.ma.On("Getenv", "CONFIG_DIR").Return("")
	suite.ma.On("Exists", path).Return(true)
	suite.ma.On("ReadFile", path).Return([]byte(yaml), nil)

	conf := load(suite.ma, &cc)

	suite.ma.AssertExpectations(suite.T())
	assert.IsType(suite.T(), make(map[string]interface{}), conf.Custom)
	assert.Equal(suite.T(), "test", cc.SomeKey)
}

func (suite *TestSuite) TestLoadParsesConfigWithCustomNilJson() {
	json := `{
	"serviceName": "test",
	"port": 8081,
	"env": "env overwrite",
	"tls": {
		"key": "some key",
		"cert": "some cert"
	},
	"custom": {
		"someKey": "test"
	}
}
`
	env := "sandbox"
	path := "config/" + env + ".json"
	yamlPath := "config/" + env + ".yml"
	suite.ma.On("Getenv", "ENV").Return(env)
	suite.ma.On("Getenv", "CONFIG_DIR").Return("")
	suite.ma.On("Exists", path).Return(true)
	suite.ma.On("Exists", yamlPath).Return(false)
	suite.ma.On("ReadFile", path).Return([]byte(json), nil)

	conf := load(suite.ma, nil)

	suite.ma.AssertExpectations(suite.T())
	assert.Equal(suite.T(), env, conf.Env, "Env must be populated in the config from the ENV not the yaml")
	assert.Equal(suite.T(), "test", conf.ServiceName, "Incorrect population of the config from the yaml - serviceName")
	assert.Equal(suite.T(), 8081, conf.Port, "Incorrect population of the config from the yaml - port")
	assert.Equal(suite.T(), "some key", conf.TLS.Key, "Incorrect population of the config from the yaml - tls.key")
	assert.Equal(suite.T(), "some cert", conf.TLS.Cert, "Incorrect population of the config from the yaml - tls.cert")
}

func (suite *TestSuite) TestLoadParsesCustomConfigJson() {
	json := `{
	"custom": {
		"someKey": "test"
	}
}
`
	env := "sandbox"
	path := "config/" + env + ".yml"
	yamlPath := "config/" + env + ".yml"
	cc := CustomConfig{}
	suite.ma.On("Getenv", "ENV").Return(env)
	suite.ma.On("Getenv", "CONFIG_DIR").Return("")
	suite.ma.On("Exists", path).Return(true)
	suite.ma.On("Exists", yamlPath).Return(false)
	suite.ma.On("ReadFile", path).Return([]byte(json), nil)

	conf := load(suite.ma, &cc)

	suite.ma.AssertExpectations(suite.T())
	assert.IsType(suite.T(), make(map[string]interface{}), conf.Custom)
	assert.Equal(suite.T(), "test", cc.SomeKey)
}

func TestConfigLoadSuit(t *testing.T) {
	suite.Run(t, new(TestSuite))
}
